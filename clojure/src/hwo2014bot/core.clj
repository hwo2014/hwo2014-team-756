(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [clojure.java.io :refer :all]
            [clojure.string :refer [join]])
  (:use hwo2014bot.protocol)
  (:use hwo2014bot.world)
  (:gen-class))


(def verbose (atom true))
(def fail-fast (atom true))

(def g-host-r1 "senna.helloworldopen.com")
(def g-host-r2 "hakkinen.helloworldopen.com")
(def g-host-r3 "webber.helloworldopen.com")
(def g-host g-host-r2)
(def g-port 8091)
(def g-botkey "bwiDZkwfrPP5IA")
(def g-pass "8h8XSuSZQh")
(def log-file "log.txt")

(def track-keimola "keimola")
(def track-germany "germany")
(def track-usa "usa")
(def track-france "france")
(def random-switch false)

(defn safe-speed
  "safe speed to enter a bend with a given angle and radius"
  [angle radius]
  (-> [[45.0  50 3]
       [45.0 100 5]
       [45.0 200 7]
       
       [22.5  50 6]
       [22.5 100 7]
       [22.5 200 8]]
      ((partial filter (fn [[a r s]] (and (= a (Math/abs angle))
                                         (= r radius)))))
      first
      (nth 2)))

(def throttle-keimola
  [[1.0 [33 34 35 36 37 38 39 0 1 2]]
   [0.0 [3]]
   [0.6 [4]]
   [0.7 [5 6]]
   [1.0 [7 8 9 10 11]]
   [0.0 [12]]
   [0.6 [13 14 15 16 17]]
   [0.75 [18 19 20 21]]
   [1.0 [22 23 24]]
   [0.0 [25]]
   [0.7 [26 27 28 29 30 31]]
   [0.5 [32]]])

(defn log [& args]
  (if @verbose
    (let [s (str (join " " args) "\n")]
      (spit "log.txt" s :append true)
      (print s))))

(defn can-switch? [world]
  (let [next (my-next-piece world)]
    (and (not (nil? (:switch next)))
         (> (my-in-piece-progress world) 90))))

(defn switch-left? [world]
  (let [b (my-next-bend world)]
    (and (can-switch? world)
         (or (and random-switch (= 0 (rand-int 2)))
             (and (:angle b) (neg? (:angle b))
                  (< 0 (my-start-lane-index world)))))))

(defn switch-right? [world]
  (let [b (my-next-bend world)]
    (and (can-switch? world)
         (or (and random-switch (= 0 (rand-int 2)))
             (and (:angle b)
                  (pos? (:angle b))
                  (< (my-start-lane-index world)
                     (dec (count (lanes world)))))))))

(defn can-turbo? [world]
  (if-let [t (:turboAvailable world)]
    (let [turbo-dist (* (-> t :data :turboDurationTicks)
                        (-> t :data :turboFactor)
                        2 (my-speed world))]
      (and (< turbo-dist (my-ahead world))
           (< (Math/abs (:angle (my-position world))) 20)))))

(defn ai-constant [world msg]
  (bot-throttle 0.5))

(defn ai-drifter [world msg]
  (let [piece (my-piece world)
        pos (my-position world)
        speed (my-speed world)
        accel (my-accel world)
        ahead (my-ahead world)
        tick (msg :gameTick)
        bend (my-next-bend world)
        perc (my-in-piece-progress world)]

    (cond

     (switch-left? world)
     (bot-switch-lane "Left")

     (switch-right? world)
     (bot-switch-lane "Right")

     (< speed 1)
     (bot-throttle 1)

     (can-turbo? world)
     (bot-turbo)

     ;; Slow down for upcoming bend
     (and bend
          (let [safe (safe-speed (:angle bend) (:radius bend))
                delta (- speed safe)
                brake-time (/ delta (Math/abs accel))
                brake-dist (* brake-time speed)]
            (> brake-dist ahead)))
     (bot-throttle 0.3)

     (:angle piece)
     (bot-throttle
      (let [a-piece (Math/abs (:angle piece))
            a-drift (Math/abs (:angle pos))
            a-relative (/ a-drift a-piece)
            radius (:radius piece)
            dd (my-delta-drift world)
            abs-dd (Math/abs dd)
            safe-speed (safe-speed a-piece radius)]
        
        (cond (<= radius 50) ;; thight bend
              (cond (< perc 5) 0
                    (> perc 95) 1
                    (< a-relative 0.25) (* 0.1 safe-speed)
                    (< a-relative 0.75) 
                    (cond (< abs-dd 0.5) (* 0.1 safe-speed)
                          (< abs-dd 2) 1
                          :else 0)
                    :else (* 0.1 safe-speed))

              (<= radius 100) ;; intermediate bend
              (cond ;(< perc 10) 0
                    (> perc 95) (* 0.1 safe-speed)
                    (< a-relative 0.5) (* 0.1 safe-speed)
                    (< a-relative 3.0)
                    (cond (< abs-dd 0.5) (* 0.1 safe-speed)
                          (< abs-dd 3) 1
                          :else 0)
                    :else (* 0.1 safe-speed))

              (<= radius 200) ;; wide bend
              (cond (< perc 20) 0
                    (> perc 80) 1
                    (< a-relative 1) 1
                    (< a-relative 3) (cond (neg? dd) 1
                                           (pos? dd) 0
                                           :else (* speed 0.1))
                    :else (* safe-speed 0.1))

              :else 0.5)))

     :else (bot-throttle 1))))

(defmulti handle-msg (fn [_ msg] (:msgType msg)))

(defmethod handle-msg "yourCar" [world msg]
  (bot-ping))

(defmethod handle-msg "gameInit" [world msg]
  (bot-ping))

(defmethod handle-msg "gameStart" [world msg]
  (bot-ping))

(defmethod handle-msg "gameEnd" [world msg]
  (bot-ping))

(defmethod handle-msg "carPositions" [world msg]
  (let [cmd (ai-drifter world msg)]
    (println cmd)
     (assoc cmd :gameTick (msg :gameTick))))

(defmethod handle-msg "crash" [world msg]
  (bot-ping))

(defmethod handle-msg "spawn" [world msg]
  (bot-ping))

(defmethod handle-msg "lapFinished" [world msg]
  (bot-ping))

(defmethod handle-msg "dnf" [world msg]
  (bot-ping))

(defmethod handle-msg "finish" [world msg]
  (bot-ping))

(defmethod handle-msg "error" [world msg]
  (bot-ping))

(defmethod handle-msg :default [world msg]
  (bot-ping))

(defn log-msg [msg]
  (condp = (:msgType msg)
    "join" (println (get-in msg [:data :name]) "joined the game")
    "yourCar" (println "Your car has the color" (get-in msg [:data :color]))
    "gameInit" (println "The game is initialzed")
    "gameStart" (println "The game has started")
    "lapFinished" (println (get-in msg [:data :car :name]) "has finished a lap")
    "finished" (println (get-in msg [:data :name]) "has finished the race")
    "gameEnd" (println "The game has ended")
    "tournamentEnd" (println "The tournament has ended")
    "crash" (println (get-in msg [:data :name]) "has crashed")
    "spawn" (println (get-in msg [:data :name]) "has spawned")
    nil))


(defn show-status [world]
  (when (not-empty (:carPositions world))
    (let [s (str " v:" (format "%.4f" (my-speed world))
                 " a:" (format "%.2f" (my-accel world))
                 " x:" (format "%.0f" (my-ahead world))
                 " y:" (format "%.0f" (my-ahead-bend world))
                 " d:" (format "%.2f" (my-drift-angle world))
                 (when (my-piece-angle world)
                   (str
;                    " p:" (format "%.1f" (my-piece-angle world))
                    " r:" (format "%.2f" (/ (my-drift-angle world) (my-piece-angle world)))))
                 " z:" (format "%.2f" (my-delta-drift world))
                 ;" l:" (my-start-lane-index world) "/" (my-end-lane-index world)
                 " %:" (format "%.0f" (my-in-piece-progress world))
                 " #:" (-> world my-position :piecePosition :pieceIndex)
                 ;" ns:" (my-next-switch world)
                 ;" nb:" (my-next-bend world)
                 " " (-> world my-piece)
                 )]
      (log s))))

(defn game-loop [channel initial]
  (spit "log.txt" "START\n")
  (let [world {}]
    (loop [cmd initial world world]
      (show-status world)
      ;(log ">> " cmd)
      (send-message channel cmd)
      (if-let [msg (read-message channel)]
        (do
          (log-msg msg)
          ;(log "<< " msg)
          (if-not (= "tournamentEnd" (:msgType msg))
            (let [world (update-world world msg)]
              (recur (handle-msg world msg) world))
            world))
        world))))

(defn start-game [host port initial-cmd]
  (try
    (let [channel (connect-client-channel host port)]
      (game-loop channel initial-cmd))
    (catch Exception e
      (println e)
      (.printStackTrace e))))


(defn -main[& [host port botname botkey]]
  (reset! fail-fast false)
  (start-game host (Integer/parseInt port) 
              (bot-join botname botkey)))

(defn run-main []
  (-main g-host "8091" "Endy" g-botkey))

(defn run [trackname]
  (start-game g-host 8091
              (bot-join-race "Endy" g-botkey 1 trackname)))

(defn quick [trackname botname cars]
  (start-game g-host 8091
              (bot-join-race botname g-botkey cars trackname)))

(defn spawn [track botname cars]
  (.start
   (Thread.
    (fn []
      (start-game g-host 8091
                  (bot-join-race botname g-botkey cars track))))))
