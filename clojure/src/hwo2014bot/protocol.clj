(ns hwo2014bot.protocol
  (:require [clojure.data.json :as json]
            [aleph.tcp :refer [tcp-client]]
            [lamina.core :refer [enqueue wait-for-message wait-for-result]]
            [gloss.core :refer [string]]))

(defn bot-join [botname botkey]
  {:msgType "join" :data {:name botname :key botkey}})

(defn bot-ping []
  {:msgType "ping" :data "ping"})

(defn bot-throttle [rate]
  {:msgType "throttle" :data rate})

(defn bot-turbo []
  {:msgType "turbo" :data "whoof whoof!!!"})

(defn bot-switch-lane [dir]
  {:msgType "switchLane" :data dir})

(defn bot-create-race [botname botkey carcount & [trackname]]
  (let [cmd {:msgType "createRace"
             :data {:botId {:name botname :key botkey}
                    :carCount carcount}}]
    (if trackname
      (assoc-in cmd [:data :trackName] trackname)
      cmd)))

(defn bot-join-race [botname botkey carcount & [trackname]]
  (let [cmd {:msgType "joinRace"
             :data {:botId {:name botname :key botkey}
                    :carCount carcount}}]
    (if trackname
      (assoc-in cmd [:data :trackName] trackname)
      cmd)))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (try
    (json->clj
     (wait-for-message channel))
    (catch Exception e
      (println e))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

