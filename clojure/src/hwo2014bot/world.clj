(ns hwo2014bot.world)

(declare my-status my-name my-piece-index
         my-piece-length)

(defn index-of [col x]
  (.indexOf col x))

(defn qualifying? [world]
  (-> world :gameInit :data :race :raceSession :durationMs))

(defn race? [world]
  (-> world :gameInit :data :race :raceSession :laps))

(defn- conj-to [world kw item]
  (assoc world kw (conj (or (kw world) []) item)))

(defmulti update-world (fn [world msg] (:msgType msg)))

(defmethod update-world "join" [world msg]
  world)

(defmethod update-world "joinRace" [world msg]
  world)

(defmethod update-world "yourCar" [world msg]
  (assoc world :yourCar msg))

(defmethod update-world "gameInit" [world msg]
  (assoc world :gameInit msg))

(defmethod update-world "gameStart" [world msg]
  (assoc world :gameStart msg))

(defmethod update-world "gameEnd" [world msg]
  (assoc world :gameEnd msg))

(defmethod update-world "crash" [world msg]
  (if (= (my-name world) (get-in msg [:data :name]))
    (conj-to world :crashes (my-status world))
    world))

(defmethod update-world "crash" [world msg]
  world)

(defmethod update-world "spawn" [world msg]
  world)

(defmethod update-world "turboAvailable" [world msg]
  (assoc world :turboAvailable msg))

(defmethod update-world "turboStart" [world msg]
  (-> world
      (assoc :turboActive true)
      (dissoc :turboAvailable)))

(defmethod update-world "turboEnd" [world msg]
  (dissoc world :turboActive))

(defmethod update-world "carPositions" [world msg]
  (let [cp (or (world :carPositions) [])
        cp2 (drop (if (< (count cp) 10) 0 1) cp)]
    (assoc world :carPositions (conj cp2 msg))))

(defmethod update-world "lapFinished" [world msg]
  (conj-to world :lapFinished msg))

(defmethod update-world :default [world msg]
  (conj-to world :unknown msg))

;; -------------------------------------
;; End of world update
;; ------------------------------------

(defn piece-index [pos]
  (get-in pos [:piecePosition :pieceIndex]))

(defn in-piece-distance [pos]
  (get-in pos [:piecePosition :inPieceDistance]))

(defn start-lane-index [pos]
  (get-in pos [:piecePosition :lane :startLaneIndex]))

(defn end-lane-index [pos]
  (get-in pos [:piecePosition :lane :endLaneIndex]))

(defn pieces [world]
  (get-in world [:gameInit :data :race :track :pieces]))

(defn lanes [world]
  (get-in world [:gameInit :data :race :track :lanes]))

(defn cars [world]
  (get-in world [:gameInit :data :race :cars]))

(defn select-position [col name]
  (first
   (filter #(= name (get-in % [:id :name])) col)))

(defn positions [world name]
  (map #(select-position % name)
       (map :data (world :carPositions))))

(defn piece-length [world pos]
  (let [piece ((pieces world) (piece-index pos))
        lane ((lanes world) (start-lane-index pos))]
    (if (:angle piece)
      (let [a (Math/abs (:angle piece))
            s (Math/signum (:angle piece))
            d (:distanceFromCenter lane)
            r (- (:radius piece) (* s d))]
        (* Math/PI r (/ a 180)))
      (:length piece))))

(defn my-name [world]
  (get-in world [:yourCar :data :name]))

(defn my-positions [world]
  (into [] (positions world (my-name world))))

(defn my-position [world]
  (last (my-positions world)))

(defn my-index [world]
  (piece-index (my-position world)))

(defn my-piece [world]
  ((pieces world) (my-index world)))

(defn my-next-pieces [world]
  (let [pos (my-position world)
        pieces (pieces world)
        laps (or (get-in world [:gameInit :data :race :raceSession :laps])
                 100)
        lap (get-in pos [:piecePosition :lap])]
    (concat
     (drop (get-in pos [:piecePosition :pieceIndex]) pieces)
     (take (- laps lap) (cycle pieces))
     [{:length 10000}])))

(defn distance [world a b]
  (let [ixa (piece-index a)
        ixb (piece-index b)]
    (cond
     (= ixa ixb) (- (in-piece-distance b) (in-piece-distance a))
     :else       (+ (piece-length world a)
                    (- (in-piece-distance a))
                    (in-piece-distance b)))))

(defn my-last-positions [world n]
  (->> world
       :carPositions
       (take-last n)
       (mapcat :data)
       (filter #(= (my-name world) (get-in % [:id :name])))))

(defn my-speed [world]
  (let [[a b] (my-last-positions world 2)]
    ;(println "my-speed" a b)
    (if b
      (distance world a b)
      0.0)))

(defn my-accel [world]
  (let [[a b c] (my-last-positions world 3)]
    (if c
      (- (distance world b c) (distance world a b))
      0.0)))

(defn my-accel-2 [world]
  (let [[a b c d] (my-last-positions world 4)]
    (if d
      (let [g0 (- (distance world a b))
            g1 (- (distance world b c))
            g2 (- (distance world c d))]
        (- (- g2 g1) (- g1 g0)))
      0.0)))

(defn my-ratio [world]
  (let [[a b c] (my-last-positions world 3)]
    (if c
      (/ (- (distance world b c) (distance world a b))
         (distance world a b))
      0.0)))

(defn my-ahead [world]
  (let [pos (my-position world)
        piece (my-piece world)
        next (my-next-pieces world)]
    (if (:length piece)
      (+ (:length piece)
         (- (in-piece-distance pos))
         (apply + (map :length (take-while :length next))))
      0.0)))

(defn bend-length [piece]
  (if (:angle piece)
    (* Math/PI (:radius piece) (Math/abs (:angle piece)) (/ 1 180.0))
    0.0))

(defn my-ahead-bend [world]
  (let [pos (my-position world)
        piece (my-piece world)
        next (my-next-pieces world)]
    (if (:angle piece)
      (+ (bend-length piece)
         (- (in-piece-distance pos))
         (apply + (map bend-length (take-while :angle next))))
      0.0)))

(defn my-next-piece-with [world kw]
  (->> world
       my-next-pieces
       (filter kw)
       first))

(defn my-next-switch [world]
  (my-next-piece-with world :switch))

(defn my-next-bend [world]
  (my-next-piece-with world :angle))

(defn my-next-piece [world]
  (first (my-next-pieces world)))

(defn my-drift-angle [world]
  (-> world my-position :angle))

(defn my-piece-angle [world]
  (-> world my-piece :angle))

(defn my-delta-drift [world]
  (let [pp (my-positions world)
        c (count pp)]
    (if (>= c 2)
      (- (:angle (pp (dec c)))
         (:angle (pp (dec (dec c))))))))

(defn my-piece-index [world]
  (-> world my-piece ((partial index-of (pieces world))))) 

(defn my-start-lane-index [world]
  (-> world my-position :piecePosition :lane :startLaneIndex))

(defn my-end-lane-index [world]
  (-> world my-position :piecePosition :lane :endLaneIndex))

(defn my-in-piece-distance [world]
  (-> world my-position :piecePosition :inPieceDistance))

(defn my-start-lane [world]
  ((lanes world) (my-start-lane-index world)))

(defn my-end-lane [world]
  ((lanes world) (my-end-lane-index world)))

(defn my-radius [world]
  (let [piece (my-piece world)
        lane (my-start-lane world)]
    (when (:angle piece)
      (+ (:radius piece)
         (:distanceFromCenter lane)))))

(defn my-in-piece-progress [world]
  (/ (* 100 (my-in-piece-distance world))
     (piece-length world (my-position world))))

